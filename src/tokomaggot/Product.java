/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tokomaggot;

/**
 *
 * @author ITDEV
 */
public class Product {
    private static String id;
    public static String product_price;
    public static String product_name;
    public static String product_category;
    public static String product_exp;
    public static String product_desc;
    public static String product_stock;
    public static String product_satuan;
    public static String product_address;
    public static String product_insurance;
    public static String product_merk;
    public static String product_contact;
    public static String id_shop;

    public static String getId() {
        return id;
    }

    public static void setId(String id) {
        Product.id = id;
    }

    public static String getProduct_price() {
        return product_price;
    }

    public static void setProduct_price(String product_price) {
        Product.product_price = product_price;
    }

    public static String getProduct_name() {
        return product_name;
    }

    public static void setProduct_name(String product_name) {
        Product.product_name = product_name;
    }

    public static String getProduct_category() {
        return product_category;
    }

    public static void setProduct_category(String product_category) {
        Product.product_category = product_category;
    }

    public static String getProduct_exp() {
        return product_exp;
    }

    public static void setProduct_exp(String product_exp) {
        Product.product_exp = product_exp;
    }

    public static String getProduct_desc() {
        return product_desc;
    }

    public static void setProduct_desc(String product_desc) {
        Product.product_desc = product_desc;
    }

    public static String getProduct_stock() {
        return product_stock;
    }

    public static void setProduct_stock(String product_stock) {
        Product.product_stock = product_stock;
    }

    public static String getProduct_satuan() {
        return product_satuan;
    }

    public static void setProduct_satuan(String product_satuan) {
        Product.product_satuan = product_satuan;
    }

    public static String getProduct_address() {
        return product_address;
    }

    public static void setProduct_address(String product_address) {
        Product.product_address = product_address;
    }

    public static String getProduct_insurance() {
        return product_insurance;
    }

    public static void setProduct_insurance(String product_insurance) {
        Product.product_insurance = product_insurance;
    }

    public static String getProduct_merk() {
        return product_merk;
    }

    public static void setProduct_merk(String product_merk) {
        Product.product_merk = product_merk;
    }

    public static String getProduct_contact() {
        return product_contact;
    }

    public static void setProduct_contact(String product_contact) {
        Product.product_contact = product_contact;
    }

    public static String getId_shop() {
        return id_shop;
    }

    public static void setId_shop(String id_shop) {
        Product.id_shop = id_shop;
    }

    
    
}
