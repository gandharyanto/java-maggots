/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tokomaggot;

import java.util.List;

/**
 *
 * @author ITDEV
 */
public interface Callback {

    void onSuccess();

    void onSuccessMultipleImage(List<imageObject> images);

    void onFailure();
}
