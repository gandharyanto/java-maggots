/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tokomaggot;

/**
 *
 * @author ITDEV
 */
public class loginSession {
    private static String u_username;
    private static String u_nama;
    private static String u_role;
    private static String u_id;
    private static String u_idShop;

    public static String getU_idShop() {
        return u_idShop;
    }

    public static void setU_idShop(String u_idShop) {
        loginSession.u_idShop = u_idShop;
    }

    public static String getU_id() {
        return u_id;
    }

    public static void setU_id(String u_id) {
        loginSession.u_id = u_id;
    }

    public static String getU_username() {
        return u_username;
    }

    public static void setU_username(String u_username) {
        loginSession.u_username = u_username;
    }

    public static String getU_nama() {
        return u_nama;
    }

    public static void setU_nama(String u_nama) {
        loginSession.u_nama = u_nama;
    }

    public static String getU_role() {
        return u_role;
    }

    public static void setU_role(String u_role) {
        loginSession.u_role = u_role;
    }
}
