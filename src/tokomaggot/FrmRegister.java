/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tokomaggot;

import java.security.NoSuchAlgorithmException;
import java.sql.ResultSet;
import java.util.Arrays;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JComboBox;
import javax.swing.JOptionPane;
import static tokomaggot.FrmLogin.sha1;

/**
 *
 * @author ITDEV
 */
public class FrmRegister extends javax.swing.JFrame {

    private String role;
    private String category;
    private String gender = "0";

    /**
     * Creates new form FrmRegister
     */
    public FrmRegister() {
        initComponents();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        lblTitle = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        tfRegisterName = new javax.swing.JTextField();
        tfRegisterBirthDate = new javax.swing.JTextField();
        tfRegisterAddress = new javax.swing.JTextField();
        tfRegisterEmail = new javax.swing.JTextField();
        tfRegisterPhone = new javax.swing.JTextField();
        cbRole = new javax.swing.JComboBox<>();
        jPanel3 = new javax.swing.JPanel();
        jLabel9 = new javax.swing.JLabel();
        tfRegisterUsername = new javax.swing.JTextField();
        jLabel14 = new javax.swing.JLabel();
        jLabel15 = new javax.swing.JLabel();
        pfPasswordConfirm = new javax.swing.JPasswordField();
        pfPassword = new javax.swing.JPasswordField();
        pnlRegisterShop = new javax.swing.JPanel();
        jLabel10 = new javax.swing.JLabel();
        tfRegisterShopName = new javax.swing.JTextField();
        jLabel11 = new javax.swing.JLabel();
        cbCategory = new javax.swing.JComboBox<>();
        jLabel12 = new javax.swing.JLabel();
        tfRegisterShopAddress = new javax.swing.JTextField();
        jLabel13 = new javax.swing.JLabel();
        tfRegisterShopDesc = new javax.swing.JTextField();
        cbGender = new javax.swing.JComboBox<>();
        btnRegisterBack = new java.awt.Button();
        btnRegisterReset = new java.awt.Button();
        btnRegister1 = new java.awt.Button();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setPreferredSize(new java.awt.Dimension(1074, 754));

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));
        jPanel1.setLayout(null);

        jPanel2.setBackground(new java.awt.Color(255, 255, 255));
        jPanel2.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));

        lblTitle.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        lblTitle.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblTitle.setText("HARAP PASTIKAN DATA YANG DIMASUKAN SUDAH BENAR DAN SESUAI");

        jLabel2.setText("NAMA LENGKAP                                    : ");

        jLabel3.setText("TANGGAL LAHIR (yyyy-MM-dd)           : ");

        jLabel4.setText("ALAMAT                                                : ");

        jLabel5.setText("JENIS KELAMIN                                     : ");

        jLabel6.setText("EMAIL                                                   : ");

        jLabel7.setText("NO. HANDPHONE                                 : ");

        jLabel8.setText("DAFTAR SEBAGAI                                : ");

        tfRegisterName.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                tfRegisterNameActionPerformed(evt);
            }
        });

        tfRegisterBirthDate.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                tfRegisterBirthDateActionPerformed(evt);
            }
        });

        tfRegisterAddress.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                tfRegisterAddressActionPerformed(evt);
            }
        });

        tfRegisterEmail.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                tfRegisterEmailActionPerformed(evt);
            }
        });

        tfRegisterPhone.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                tfRegisterPhoneActionPerformed(evt);
            }
        });

        cbRole.setMaximumRowCount(2);
        cbRole.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Penjual", "Pelanggan" }));
        cbRole.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cbRoleActionPerformed(evt);
            }
        });

        jPanel3.setBackground(new java.awt.Color(255, 255, 255));
        jPanel3.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 248, Short.MAX_VALUE)
        );

        jLabel9.setText("ULANGI PASSWORD                             : ");

        tfRegisterUsername.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                tfRegisterUsernameActionPerformed(evt);
            }
        });

        jLabel14.setText("USERNAME                                           : ");

        jLabel15.setText("PASSWORD                                          : ");

        jLabel10.setText("NAMA USAHA                                       : ");

        tfRegisterShopName.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                tfRegisterShopNameActionPerformed(evt);
            }
        });

        jLabel11.setText("KATEGORI USAHA                               : ");

        cbCategory.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Pertanian", "Peternakan Maggot BSF" }));
        cbCategory.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cbCategoryActionPerformed(evt);
            }
        });

        jLabel12.setText("ALAMAT USAHA                                  : ");

        tfRegisterShopAddress.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                tfRegisterShopAddressActionPerformed(evt);
            }
        });

        jLabel13.setText("DESKRIPSI USAHA                              : ");

        tfRegisterShopDesc.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                tfRegisterShopDescActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout pnlRegisterShopLayout = new javax.swing.GroupLayout(pnlRegisterShop);
        pnlRegisterShop.setLayout(pnlRegisterShopLayout);
        pnlRegisterShopLayout.setHorizontalGroup(
            pnlRegisterShopLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlRegisterShopLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnlRegisterShopLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(pnlRegisterShopLayout.createSequentialGroup()
                        .addComponent(jLabel10)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(tfRegisterShopName, javax.swing.GroupLayout.PREFERRED_SIZE, 219, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(pnlRegisterShopLayout.createSequentialGroup()
                        .addComponent(jLabel13)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(tfRegisterShopDesc, javax.swing.GroupLayout.PREFERRED_SIZE, 219, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(pnlRegisterShopLayout.createSequentialGroup()
                        .addComponent(jLabel12)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(tfRegisterShopAddress, javax.swing.GroupLayout.PREFERRED_SIZE, 219, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(pnlRegisterShopLayout.createSequentialGroup()
                        .addComponent(jLabel11)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(cbCategory, javax.swing.GroupLayout.PREFERRED_SIZE, 219, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        pnlRegisterShopLayout.setVerticalGroup(
            pnlRegisterShopLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlRegisterShopLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnlRegisterShopLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel10)
                    .addComponent(tfRegisterShopName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(pnlRegisterShopLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel11)
                    .addComponent(cbCategory, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(pnlRegisterShopLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel12)
                    .addComponent(tfRegisterShopAddress, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(pnlRegisterShopLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel13)
                    .addComponent(tfRegisterShopDesc, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        cbGender.setMaximumRowCount(3);
        cbGender.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "--PILIH--", "Laki-Laki", "Perempuan" }));
        cbGender.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cbGenderActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGap(153, 153, 153)
                        .addComponent(lblTitle, javax.swing.GroupLayout.PREFERRED_SIZE, 746, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGap(62, 62, 62)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(jPanel2Layout.createSequentialGroup()
                                        .addComponent(jLabel6)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                        .addComponent(tfRegisterEmail, javax.swing.GroupLayout.PREFERRED_SIZE, 219, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                        .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel2Layout.createSequentialGroup()
                                            .addComponent(jLabel7)
                                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                            .addComponent(tfRegisterPhone, javax.swing.GroupLayout.PREFERRED_SIZE, 219, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel2Layout.createSequentialGroup()
                                            .addComponent(jLabel8)
                                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                            .addComponent(cbRole, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                                    .addGroup(jPanel2Layout.createSequentialGroup()
                                        .addComponent(jLabel4)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                        .addComponent(tfRegisterAddress, javax.swing.GroupLayout.PREFERRED_SIZE, 219, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addGroup(jPanel2Layout.createSequentialGroup()
                                        .addComponent(jLabel3)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                        .addComponent(tfRegisterBirthDate, javax.swing.GroupLayout.PREFERRED_SIZE, 219, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addGroup(jPanel2Layout.createSequentialGroup()
                                        .addComponent(jLabel2)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                        .addComponent(tfRegisterName, javax.swing.GroupLayout.PREFERRED_SIZE, 219, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                .addGap(19, 19, 19))
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addComponent(jLabel5)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(cbGender, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addGap(17, 17, 17)))
                        .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addGap(18, 18, 18)
                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(jPanel2Layout.createSequentialGroup()
                                        .addComponent(jLabel9)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                        .addComponent(pfPasswordConfirm, javax.swing.GroupLayout.PREFERRED_SIZE, 219, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addGroup(jPanel2Layout.createSequentialGroup()
                                        .addComponent(jLabel15)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                        .addComponent(pfPassword, javax.swing.GroupLayout.PREFERRED_SIZE, 219, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addGroup(jPanel2Layout.createSequentialGroup()
                                        .addComponent(jLabel14)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                        .addComponent(tfRegisterUsername, javax.swing.GroupLayout.PREFERRED_SIZE, 219, javax.swing.GroupLayout.PREFERRED_SIZE))))
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addGap(8, 8, 8)
                                .addComponent(pnlRegisterShop, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                .addContainerGap(102, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lblTitle)
                .addGap(66, 66, 66)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(jPanel2Layout.createSequentialGroup()
                            .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(jLabel2)
                                .addComponent(tfRegisterName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGap(18, 18, 18)
                            .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(jLabel3)
                                .addComponent(tfRegisterBirthDate, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGap(18, 18, 18)
                            .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(jLabel4)
                                .addComponent(tfRegisterAddress, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGap(18, 18, 18)
                            .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(jLabel5)
                                .addComponent(cbGender, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGap(18, 18, 18)
                            .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(jLabel6)
                                .addComponent(tfRegisterEmail, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGap(18, 18, 18)
                            .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(jLabel7)
                                .addComponent(tfRegisterPhone, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGap(18, 18, 18)
                            .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(jLabel8)
                                .addComponent(cbRole, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addComponent(jPanel3, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel14)
                            .addComponent(tfRegisterUsername, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel15)
                            .addComponent(pfPassword, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel9)
                            .addComponent(pfPasswordConfirm, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(pnlRegisterShop, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(255, Short.MAX_VALUE))
        );

        jPanel1.add(jPanel2);
        jPanel2.setBounds(10, 11, 1054, 607);

        btnRegisterBack.setBackground(new java.awt.Color(240, 173, 78));
        btnRegisterBack.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnRegisterBack.setFont(new java.awt.Font("Dialog", 1, 12)); // NOI18N
        btnRegisterBack.setLabel("KEMBALI");
        btnRegisterBack.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnRegisterBackActionPerformed(evt);
            }
        });
        jPanel1.add(btnRegisterBack);
        btnRegisterBack.setBounds(10, 640, 120, 40);

        btnRegisterReset.setBackground(new java.awt.Color(208, 118, 78));
        btnRegisterReset.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnRegisterReset.setFont(new java.awt.Font("Dialog", 1, 12)); // NOI18N
        btnRegisterReset.setLabel("RESET");
        btnRegisterReset.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnRegisterResetActionPerformed(evt);
            }
        });
        jPanel1.add(btnRegisterReset);
        btnRegisterReset.setBounds(940, 640, 120, 40);

        btnRegister1.setBackground(new java.awt.Color(92, 192, 222));
        btnRegister1.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnRegister1.setFont(new java.awt.Font("Dialog", 1, 12)); // NOI18N
        btnRegister1.setLabel("DAFTAR");
        btnRegister1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnRegister1ActionPerformed(evt);
            }
        });
        jPanel1.add(btnRegister1);
        btnRegister1.setBounds(780, 640, 120, 40);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void tfRegisterNameActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_tfRegisterNameActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_tfRegisterNameActionPerformed

    private void tfRegisterBirthDateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_tfRegisterBirthDateActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_tfRegisterBirthDateActionPerformed

    private void tfRegisterAddressActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_tfRegisterAddressActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_tfRegisterAddressActionPerformed

    private void tfRegisterEmailActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_tfRegisterEmailActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_tfRegisterEmailActionPerformed

    private void tfRegisterPhoneActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_tfRegisterPhoneActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_tfRegisterPhoneActionPerformed

    private void tfRegisterShopAddressActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_tfRegisterShopAddressActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_tfRegisterShopAddressActionPerformed

    private void tfRegisterUsernameActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_tfRegisterUsernameActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_tfRegisterUsernameActionPerformed

    private void tfRegisterShopNameActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_tfRegisterShopNameActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_tfRegisterShopNameActionPerformed

    private void tfRegisterShopDescActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_tfRegisterShopDescActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_tfRegisterShopDescActionPerformed

    private void btnRegisterBackActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnRegisterBackActionPerformed
        pfPassword.setText("");
        pfPasswordConfirm.setText("");
        tfRegisterAddress.setText("");
        tfRegisterBirthDate.setText("");
        tfRegisterEmail.setText("");
        tfRegisterName.setText("");
        tfRegisterPhone.setText("");
        tfRegisterShopAddress.setText("");
        tfRegisterShopDesc.setText("");
        tfRegisterShopName.setText("");
        tfRegisterUsername.setText("");
        this.dispose();
    }//GEN-LAST:event_btnRegisterBackActionPerformed

    private void btnRegisterResetActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnRegisterResetActionPerformed
        pfPassword.setText("");
        pfPasswordConfirm.setText("");
        tfRegisterAddress.setText("");
        tfRegisterBirthDate.setText("");
        tfRegisterEmail.setText("");
        tfRegisterName.setText("");
        tfRegisterPhone.setText("");
        tfRegisterShopAddress.setText("");
        tfRegisterShopDesc.setText("");
        tfRegisterShopName.setText("");
        tfRegisterUsername.setText("");
    }//GEN-LAST:event_btnRegisterResetActionPerformed

    private void btnRegister1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnRegister1ActionPerformed
        try {
            String name = tfRegisterName.getText();
            String birthPlace = "";
            String birthDate = tfRegisterBirthDate.getText();
            String address = tfRegisterAddress.getText();
            String email = tfRegisterEmail.getText();
            String username = tfRegisterUsername.getText();

            String password = sha1(Arrays.toString(pfPassword.getPassword()));
            String phone = tfRegisterPhone.getText();
            String shopName = tfRegisterShopName.getText();
            String shopAddress = tfRegisterShopAddress.getText();
            String shopDesc = tfRegisterShopDesc.getText();
            String shopType;
            if (gender.equalsIgnoreCase("0")) {
                JOptionPane.showMessageDialog(null, "Pilih Gender Terlebih dahulu");
            } else if (category.equals("")) {
                JOptionPane.showMessageDialog(null, "Pilih Kategori Usaha Terlebih dahulu");
            } else {
                boolean isSuccess = new dbConnection().InsertUserregister(name, birthPlace, birthDate, address, gender, email, username,
                        password, role, phone, shopName, category, shopAddress, shopDesc, "");

                if (isSuccess) {
                    loginSession.setU_username(username);
                    loginSession.setU_nama(name);
                    loginSession.setU_role(role);
                    new FrmMenu().setVisible(true);
                    this.dispose();
                } else {
                    JOptionPane.showMessageDialog(null, "Maaf, Registrasi Gagal. Silakan mencoba kembali.");

                }
            }
        } catch (NoSuchAlgorithmException ex) {
            Logger.getLogger(FrmRegister.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_btnRegister1ActionPerformed

    private void cbRoleActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cbRoleActionPerformed
        JComboBox comboBox = (JComboBox) evt.getSource();
        System.out.println(comboBox.getSelectedItem());
        if (comboBox.getSelectedIndex() == 1) {
            role = comboBox.getSelectedItem().toString();
            category = "";
            pnlRegisterShop.setVisible(false);
        } else {
            role = comboBox.getSelectedItem().toString();
            pnlRegisterShop.setVisible(true);
        }
    }//GEN-LAST:event_cbRoleActionPerformed

    private void cbGenderActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cbGenderActionPerformed
        JComboBox comboBox = (JComboBox) evt.getSource();
        System.out.println(comboBox.getSelectedItem());
        if (comboBox.getSelectedIndex() != 0) {
            gender = comboBox.getSelectedItem().toString();
        } else {
            JOptionPane.showMessageDialog(null, "Pilih Gender Terlebih dahulu");

        }
    }//GEN-LAST:event_cbGenderActionPerformed

    private void cbCategoryActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cbCategoryActionPerformed
        JComboBox comboBox = (JComboBox) evt.getSource();
        System.out.println(comboBox.getSelectedItem());
        if (comboBox.getSelectedIndex() == 1) {
            category = comboBox.getSelectedItem().toString();
        } else {
            category = comboBox.getSelectedItem().toString();
        }
    }//GEN-LAST:event_cbCategoryActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(FrmRegister.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(FrmRegister.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(FrmRegister.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(FrmRegister.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new FrmRegister().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private java.awt.Button btnRegister1;
    private java.awt.Button btnRegisterBack;
    private java.awt.Button btnRegisterReset;
    private javax.swing.JComboBox<String> cbCategory;
    private javax.swing.JComboBox<String> cbGender;
    private javax.swing.JComboBox<String> cbRole;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JLabel lblTitle;
    private javax.swing.JPasswordField pfPassword;
    private javax.swing.JPasswordField pfPasswordConfirm;
    private javax.swing.JPanel pnlRegisterShop;
    private javax.swing.JTextField tfRegisterAddress;
    private javax.swing.JTextField tfRegisterBirthDate;
    private javax.swing.JTextField tfRegisterEmail;
    private javax.swing.JTextField tfRegisterName;
    private javax.swing.JTextField tfRegisterPhone;
    private javax.swing.JTextField tfRegisterShopAddress;
    private javax.swing.JTextField tfRegisterShopDesc;
    private javax.swing.JTextField tfRegisterShopName;
    private javax.swing.JTextField tfRegisterUsername;
    // End of variables declaration//GEN-END:variables
}
